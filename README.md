# 构建后自动压缩 PNG

## 介绍

[Cocos Creator 编辑器插件] 项目构建完成之后自动压缩 PNG 资源

- 压缩引擎：[pngquant 2.12.5](https://pngquant.org/)



## 截图

![screenshot](https://gitee.com/ifaswind/image-storage/raw/master/ccc-auto-compress/screenshot.png)



## 环境

平台：Windows、Mac

引擎：Cocos Creator 2.x.x（理论上通用）



## 说明

1. 将插件文件夹 `ccc-auto-compress` 放置在 `${用户目录}/.CocosCreator/packages` 目录下即可
3. 本插件默认禁用，需自行启用
2. 点击顶部菜单栏的 **[ 扩展 --> 自动压缩 PNG ]** 打开插件配置面板
4. 配置文件存放位置：`${项目目录}/local/ccc-png-auto-compress.json`



---



# 公众号

## 菜鸟小栈

我是陈皮皮，这是我的个人公众号，专注但不仅限于游戏开发、前端和后端技术记录与分享。

每一篇原创都非常用心，你的关注就是我原创的动力！

> Input and output.

![](http://qn.chenpipi.cn/image/weixin/official-account.png)